import os
import logging
import pytest

from .fixtures import test_client, init_database

def test_welcome_page_contents(test_client, init_database):
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/auth/')
    logging.info(response.data)
    assert response.status_code == 200
    assert b"Authentication Service" in response.data
    assert b"Don't have an account?" in response.data
    assert b"Sign Up" in response.data

def test_invalid_login(test_client, init_database):
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.post('/auth/',
                                data=dict(username='TESTUSER1234', password='SecurePassword123'),
                                follow_redirects=True)
    logging.info(response.data)
    assert response.status_code == 200
    assert b"Username or password invalid." in response.data

def test_sign_up_content(test_client, init_database):
    response = test_client.get('/auth/register')
    logging.info(response.data)
    assert response.status_code == 200
    assert b"Register" in response.data
    assert b"Already have an account?" in response.data
    assert b"Sign In" in response.data

def test_sign_up(test_client, init_database):
    response = test_client.post('/auth/register',
                                data=dict(first_name='FirstNameUser', last_name='LastNameUser', username='UsernameUser', email="user@email.de", password="Secure123", password_repeat="Secure123"),
                                follow_redirects=True)
    logging.info(response.data)
    assert response.status_code == 200
    assert b"You are logged in as:" in response.data
    assert b"FirstNameUser" in response.data

def test_sign_up_invalid_password_missmatch(test_client, init_database):
    response = test_client.post('/auth/register',
                                data=dict(first_name='FirstNameUser', last_name='LastNameUser', username='UsernameUser', email="user@email.de", password="Secure123", password_repeat="Missmatch"),
                                follow_redirects=True)
    logging.info(response.data)
    assert response.status_code == 200
    assert b"Passwords do not match" in response.data

def test_login(test_client, init_database):
    response = test_client.post('/auth/',
                                data=dict(username='UsernameUser', password='Secure123'),
                                follow_redirects=True)
    logging.info(response.data)
    assert response.status_code == 200
    assert b"You are logged in as:" in response.data
    assert b"FirstNameUser" in response.data

def test_welcome_page_after_login(test_client, init_database):
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/auth/')
    logging.info(response.data)
    assert response.status_code == 200
    assert b"You are logged in as:" in response.data
    assert b"FirstNameUser" in response.data

def test_logout(test_client, init_database):
    response = test_client.get('/auth/logout')
    logging.info(response.data, follow_redirects=True)
    assert response.status_code == 302
    assert b"You should be redirected automatically" in response.data

def test_welcome_page_after_logout(test_client, init_database):
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/auth/')
    logging.info(response.data)
    assert response.status_code == 200
    assert b"Authentication Service" in response.data
    assert b"Don't have an account?" in response.data
    assert b"Sign Up" in response.data
