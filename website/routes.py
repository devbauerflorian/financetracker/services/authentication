import time
from flask import Blueprint, request, session, flash
from flask import render_template, redirect, jsonify
from werkzeug.security import gen_salt
from authlib.integrations.flask_oauth2 import current_token
from authlib.oauth2 import OAuth2Error
from .models import db, User, OAuth2Client, OAuth2Token
from .oauth2 import authorization, require_oauth


bp = Blueprint('home', __name__, static_folder='static', static_url_path='auth/static')


def current_user():
    """
    Helper function used to check if a user is logged in.
    """
    if 'id' in session:
        uid = session['id']
        return User.query.get(uid)
    return None


def split_by_crlf(s):
    return [v for v in s.splitlines() if v]

@bp.route('/', methods=('GET', 'POST'))
def home():
    """
    Displayes the main page of the OAuth server

    :param username: POST: username
    :param password: POST: password
    :returns: Redirects to Login Page if no user is logged in, otherwise the user homescreen is rendered.
    """
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=username).first()
        if not user:
            # User not found
            print(f"User {username} not found.")
            flash("Username or password invalid.")
            return redirect('/auth/')
        password_valid = user.check_password(password)
        print(f"Password valid: {password_valid}")
        if not password_valid:
             # User not found
            print(f"Password for user {username} not found.")
            flash("Username or password invalid.")
            return redirect('/auth/')
        # User + Password valid
        session['id'] = user.id
        return redirect('/auth/')
    user = current_user()
    if user:
        clients = OAuth2Client.query.filter_by(user_id=user.id).all()
    else:
        clients = []
    return render_template('home.html', user=user, clients=clients)


@bp.route('/logout')
def logout():
    """
    Logout user and close sesssion.
    """
    del session['id']
    return redirect('/auth/')

@bp.route('/update_user', methods=('GET', 'POST'))
def update_user():
    """
    Update user password.
    Store new password as encrypted hash.

    :param username: POST: password_old
    :param password: POST: password_new
    :param password: POST: password_repeat
    """
    user = current_user()
    if user is None:
        flash("Please login first.")
        return redirect('/auth')

    if request.method == 'GET':
        return render_template('update_user.html', user=user)
    password_old = request.form.get('password_old')
    password_new = request.form.get('password_new')
    password_new_repeat = request.form.get('password_repeat')


    if password_old == "":
        flash("Old password is missing.")
        return redirect('/auth/update_user')

    if password_new == "" or password_new_repeat  == "":
        flash("New password is missing.")
        return redirect('/auth/update_user')

    if password_new != password_new_repeat:
        flash("New passwords do not match.")
        return redirect('/auth/update_user')

    if password_old == "":
        flash("Old password is missing.")
        return redirect('/auth/register')

    password_valid = user.check_password(password_old)
    print(f"Password valid: {password_valid}")
    if not password_valid:
            # User not found
        flash("Old password invalid.")
        return redirect('/auth/update_user')
            
    user.password = user.hash_password(password_new_repeat)
    db.session.commit()

    return redirect('/auth/')

@bp.route('/client_information', methods=('GET', 'POST'))
def client_information():
    """
    Request user information about a logged in user.
    
    :returns: Render user client information page
    """
    user = current_user()
    if user:
        clients = OAuth2Client.query.filter_by(user_id=user.id).all()
    else:
        clients = []
    return render_template('client_information.html', user=user, clients=clients)



@bp.route('/register', methods=('GET', 'POST'))
def register():
    """
    Register a new user.

    :param username: POST: username
    :param password: POST: password
    :param password: POST: password_repeat
    :param username: POST: first_name
    :param password: POST: last_name
    :param password: POST: email

    :returns: If all parameters are valid a new user is created and the user gets redirected to the user page.
    """
    if request.method == 'GET':
        return render_template('register.html')
    username = request.form.get('username')
    password = request.form.get('password')
    password_repeat = request.form.get('password_repeat')
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    email = request.form.get('email')

    if username == "":
        flash("Username is missing")
        return redirect('/auth/register')
    if password == "" or password_repeat == "":
        flash("Password is missing.")
        return redirect('/auth/register')
    if first_name == "":
        flash("First name is missing.")
        return redirect('/auth/register')
    if last_name == "":
        flash("Last name is missing.")
        return redirect('/auth/register')
    if email == "":
        flash("Email is missing.")
        return redirect('/auth/register')

    if password != password_repeat:
        flash("Passwords do not match.")
        return redirect('/auth/register')

    user = User.query.filter_by(username=username).first()

    # User already taken
    if user:
        flash("Username already taken.")
        return redirect('/auth/register')

    if not user:
        user = User(username=username,password=password,email=email,first_name=first_name,last_name=last_name)
        db.session.add(user)
        db.session.commit()
    session['id'] = user.id
    return redirect('/auth/')

@bp.route('/create_client', methods=('GET', 'POST'))
def create_client():
    """
    Register a new OAUTH2 client application for a user.
    """
    user = current_user()
    if not user:
        return redirect('/auth/')
    if request.method == 'GET':
        return render_template('create_client.html')

    client_id = gen_salt(24)
    client_id_issued_at = int(time.time())
    client = OAuth2Client(
        client_id=client_id,
        client_id_issued_at=client_id_issued_at,
        user_id=user.id,
    )

    if client.token_endpoint_auth_method == 'none':
        client.client_secret = ''
    else:
        client.client_secret = gen_salt(48)

    form = request.form
    client_metadata = {
        "client_name": form["client_name"],
        "client_uri": form["client_uri"],
        "grant_types": split_by_crlf(form["grant_type"]),
        "redirect_uris": split_by_crlf(form["redirect_uri"]),
        "response_types": split_by_crlf(form["response_type"]),
        "scope": form["scope"],
        "token_endpoint_auth_method": form["token_endpoint_auth_method"]
    }
    client.set_client_metadata(client_metadata)
    db.session.add(client)
    db.session.commit()
    return redirect('/auth/')


@bp.route('/oauth/authorize', methods=['GET', 'POST'])
def authorize():
    """
    Login function for the main user page.

    :param username: POST: username
    :param password: POST: password

    :returns: Returns the user to the user page if all provided credentials are valid. Otherwise an error message is displayed.
    """
    user = current_user()
    print(f"User: {user}")
    if request.method == 'GET':
        try:
            grant = authorization.validate_consent_request(end_user=user)
        except OAuth2Error as error:
            print(error.error)
            return error.error
        return render_template('authorize.html', user=user, grant=grant)
    if user is None:
        print("No active user")
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=username).first()
        if not user:
            # User not found
            print(f"User {username} not found.")
            flash("Username or password invalid.")
            return redirect(request.url)
        password_valid = user.check_password(password)
        print(f"Password valid: {password_valid}")
        if not password_valid:
                # User not found
            print(f"Password for user {username} not found.")
            flash("Username or password invalid.")
            return redirect(request.url)
    # User + Password valid
    checked = request.form.get('confirm')
    print(checked)
    if checked:
        grant_user = user
    else:
        flash("Access rights not confirmed.")
        return redirect(request.url)
    return authorization.create_authorization_response(grant_user=grant_user)


@bp.route('/oauth/token', methods=['POST'])
def issue_token():
    """
    Create a new OAUTH token for a user
    """
    return authorization.create_token_response()


@bp.route('/oauth/revoke', methods=['POST'])
def revoke_token():
    """
    Revoke a OAUTH token
    """
    return authorization.create_endpoint_response('revocation')


@bp.route('/api/user_information')
def user_information():
    """
    MAIN API function used to request information about a user. This function is used by other services to access private user informations (e.g. Name).
    
    :param access_token: User OAUTH access token

    :returns: User information about user that matches access token. If the token is invalid a corresponding response is returned.
    """
    from datetime import datetime
    access_token = request.args.get('access_token')
    if access_token is None:
        print(f"Error: Invalid request.")
    print(f"/api/token_information/: request for token: {access_token} received.")
    token_info = OAuth2Token.query.filter_by(access_token=access_token).first()
    if token_info is None:
        # Token unknown
        print(f"Error: Token {access_token} is unknown.")
        return jsonify(error="invalid_token", error_description="Provided token unknown.")
    if token_info.revoked == 1:
        # Token is revoked
        print(f"Error: Token {access_token} revoked.")
        return jsonify(error="invalid_token", error_description="Provided token revoked.")
    time_now = int(time.time())
    time_end = token_info.issued_at + token_info.expires_in
    if not time_now < time_end:
        # Token is expired
        print(f"Error: Token {access_token} expired.")
        return jsonify(error="invalid_token", error_description="Provided token expired.")

    # Check scope
    if not "user_information" in token_info.scope:
        print(f"Error: Requested scope: user_information not in {token_info.scope} (Token: {access_token})")
        return jsonify(error="invalid_scope", error_description="Missing authorization")

    # Fetch user information
    user_info = User.query.filter_by(id=token_info.user_id).first()
    if user_info is None:
        # No user found
        print(f"Error: Could not find user information for id {token_info.user_id}. (Token: {access_token})")
        return jsonify(error="invalid_user", error_description="No recognized user for provided token.")

    # Token is valid and can be used
    return jsonify(error="success", error_description="", username=user_info.username, first_name=user_info.first_name, last_name=user_info.last_name, email=user_info.email,user_admin_rights=user_info.user_admin_rights, admin_rights=user_info.admin_rights)

@bp.route('/api/token_information')
def token_information():
    """
    MAIN API function used to request information about a user token. This function is used by other services to check if tokens and scopes are valid.
    
    :param access_token: User OAUTH access token

    :returns: User information about user that matches access token. If the token is invalid a corresponding response is returned.
    """
    from datetime import datetime
    access_token = request.args.get('access_token')
    if access_token is None:
        print(f"Error: Invalid request.")
        return jsonify(error="invalid_request", error_description="Invalid request. Provide access access_token and required_scope")

    print(f"/api/token_information/: request for token: {access_token} received.")
    token_info = OAuth2Token.query.filter_by(access_token=access_token).first()
    if token_info is None:
        # Token unknown
        print(f"Error: Token {access_token} is unknown.")
        return jsonify(error="invalid_token", error_description="Provided token unknown.")
    if token_info.revoked == 1:
        # Token is revoked
        print(f"Error: Token {access_token} revoked.")
        return jsonify(error="invalid_token", error_description="Provided token revoked.")
    time_now = int(time.time())
    time_end = token_info.issued_at + token_info.expires_in
    if not time_now < time_end:
        # Token is expired
        print(f"Error: Token {access_token} expired.")
        return jsonify(error="invalid_token", error_description="Provided token expired.")

    # Fetch user information
    user_info = User.query.filter_by(id=token_info.user_id).first()
    if user_info is None:
        # No user found
        print(f"Error: Could not find user information for id {token_info.user_id}. (Token: {access_token})")
        return jsonify(error="invalid_user", error_description="No recognized user for provided token.")

    # Token is valid and can be used
    return jsonify(error="success", error_description="", token_scope=token_info.scope, token_type=token_info.token_type, client_id=token_info.client_id, user_id=user_info.id, encryption_key=user_info.encryption_key, user_admin_rights=user_info.user_admin_rights, admin_rights=user_info.admin_rights)
