from website.app import create_app
import os

username = os.environ['AUTHENTICATION_DB_USERNAME']
password = os.environ['AUTHENTICATION_DB_PASSWORD']
address = os.environ['AUTHENTICATION_DB_ADDRESS']
connection_string = 'mysql+mysqldb://' + username + ':' + password + '@' + address

app = create_app({
    'SECRET_KEY': 'secret',
    'OAUTH2_REFRESH_TOKEN_GENERATOR': True,
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'SQLALCHEMY_DATABASE_URI': connection_string,
})


     