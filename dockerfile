FROM python:3.7-alpine3.11

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN set -e; \
  apk update \
  && apk add --virtual .build-deps gcc python3-dev musl-dev libffi-dev curl

RUN curl https://sh.rustup.rs -sSf | /bin/sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

RUN set -e; \
        apk add --no-cache --virtual .build-deps \
                gcc \
                libc-dev \
                linux-headers \
                mariadb-dev \
                python3-dev \
                postgresql-dev

COPY . /usr/src/app

RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev

RUN apk add --no-cache \
    build-base \
    && pip install gevent==20.9.0 \
    && apk del build-base

RUN pip3 install -r requirements.txt

RUN pip3 install gunicorn
RUN pip3 install gevent

RUN pip3 install mysqlclient
RUN pip3 install flask-cors
RUN pip3 install flask-behind-proxy
RUN pip3 install pyopenssl
RUN pip3 install cryptography
ENV AUTHLIB_INSECURE_TRANSPORT=1

EXPOSE 8011

CMD ["gunicorn", "--bind", "0.0.0.0:8011", "-w", "2", "-k", "gevent", "app:app"]

