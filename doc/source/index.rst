.. OAuth2-Server documentation master file, created by
   sphinx-quickstart on Mon Nov 16 23:12:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OAuth2 Server Documentation
=========================================

In the following sections you can find all main routes provided by the OAuth2 server module.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: website.routes
   :members:
   :private-members:
   :special-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
